#!/usr/bin/env python

import os, time, sys, random

fifo = 'fifo'

def feeder():
    pipe = os.open(fifo, os.O_WRONLY)
    Z = 0
    while 1:
        time.sleep(Z)
        X = random.uniform(0,1)
        Y = random.uniform(0,1)
        os.write(pipe, '%.5f %.5f %d\n' % (X, Y, Z))
        Z = (Z+1) % 5

def reader():
    pipe = open(fifo, 'r')             # open fifo as stdio object
    while 1:
        line = pipe.readline()[:-1]    # blocks until data sent
        print 'Received "%s" at %s' % (line, time.time(  ))

if __name__ == '__main__':
    if not os.path.exists(fifo):
        os.mkfifo(fifo)                # create a named pipe file
    if len(sys.argv) == 1:
        reader()                       # run as parent if no args
    else:                              # else run as child process
        feeder()

