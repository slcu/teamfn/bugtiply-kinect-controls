# BUGTIPLY KINECT INTERACTIVE PROGRAM

This code is the base for interacting with the evolutionary game **bugtiply** and can be used as a template for further interactive installations.

## Description

 bugtiply.py implements 4 actions which are used in the game to copy, kill, restart and move the cursor in the game.
 
 Bugtiply.py will create a pipe called `fifo` in the current working directory,
 to which it will write. Cytosim from another terminal will read `fifo`.
 
 In short, images are collected from the Kinect.
 background is substracted. Connected components are isolated, their centroid is calculated, area, convex hull and contour to obtain a bounding box.
 The hand circularity, convexity and box aspect ratio which can be used to implement a set of commands. 
 The normalised centroid coordinates and the command are then witten to a `fifo` pipe which is read by the game.

## Running

One needs to open two terminal windows:

First, start the bugtiply.py in Window 1:

    python3 bugtiply.py
    
Second, start 'cytosim' in Window 2:

    ./circus < fifo

## Dependencies

The script `bugtiply.py` relies on:

    - [libfreenect2](https://github.com/OpenKinect/libfreenect2) 
    - [pylibfreenect2](https://github.com/r9y9/pylibfreenect2)
    - [matplotlib](https://matplotlib.org)
    - [OpenCV](https://opencv.org/).


## Installation

### Create a virtual environment for Python:

    python3 -m venv env
    
Load the environment:

    source env/bin/activate
    
To install Python modules:

    python3 -m pip install [MODULE]

With all these modules installed, bugtiply was working on 4.6.2024:

    contourpy       1.2.1
    cycler          0.12.1
    Cython          3.0.10
    drawnow         0.72.5
    fonttools       4.53.0
    kiwisolver      1.4.5
    matplotlib      3.9.0
    nose            1.3.7
    numpy           1.26.4
    opencv-python   4.9.0.80
    packaging       24.0
    pillow          10.3.0
    pip             24.0
    pylibfreenect2  0.1.5.dev0
    pyparsing       3.1.2
    python-dateutil 2.9.0.post0
    setuptools      65.5.0
    six             1.16.0

### install libfreenect2:

    [source env/bin/activate]
    cd code/libfreenect2
    mkdir build
    cd build
    cmake ..
    make
    make install

--> this should install `libfreenect2` in /usr/local/lib
You may need root access for this.

For a program to find the library, you need to change your environment:

    export DYLD_LIBRARY_PATH=/usr/local/lib
    
    [this can be included in .zprofile if you use zsh]

### install pylibfreenect2:

For some reason, pip install pylibfreenect2 failed, 
but it is possible to install manually:

Installation requires cython. 
You also need to copy libfreenect2` into pylibreenect2:

    cp -r libfreenect2/build/lib pylibfreenect2/.
    cp -r libfreenect2/include/libfreenect2 pylibfreenect2/include/.

To install manually:

    [source env/bin/activate]
    cd code/pylibfreenect2
    python3 setup.py install

## Troubleshooting

1. Use `libfreenect2` tool Protonect to check that the Kinect is responding.

2. Check that Cytosim's tool `circus` is working correctly without the pipe.

3. If the library cannot be found, check your environment:

    export DYLD_LIBRARY_PATH=/usr/local/lib

## Files

* bugtiply.py : latest version (3.6.2024)
* bugtiply_cv.py : version used in June 2022

The two versions differ in the way the Kinect image is displayed onscreen: using OpenCV or MatPlotLib.

