
import cv2 as cv
import sys, os
import numpy as np

from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame
from pylibfreenect2 import createConsoleLogger, setGlobalLogger
from pylibfreenect2 import LoggerLevel

#orientation of the hand: 1 = vertical
HAND = 0
#number of pixels to be considered
area = 600
# number of pixels discarded at border of image:
g1 = 64
g2 = 92

def change_g2(value):
    global g2
    g2 = value

def change_g1(value):
    global g1
    g1 = value

def change_area(value):
    global area
    area = value

def computeMoments(img, H, W):
    c=0
    s=0.
    sy=0
    sx=0
    sxx=0
    syy=0
    for i in range(H):
        for k in range(W):
            w=img[i,k]
            if w>0.0:
                c += 1
                s += w
                sx+=w*i
                sy+=w*k
                sxx+=w*i*i
                syy+=w*k*k
    if c>0:
        sx=sx/s
        sy=sy/s
        sxx=sxx/s-sx*sx
        syy=syy/s-sy*sy
        s = s/c
    return ( s, sy, sx, syy, sxx )


def computeCutoff(data, arg):
    hist = cv.calcHist([img],[0],None,[256],[0,256])
    bins = np.linspace(500, 1500, 500)
    hist, dummy = np.histogram(data, bins)
    i=0
    sum=0
    for k in hist:
        #print(i,k,sum)
        i += 1
        sum += k
        if sum >= arg:
            break
    return bins[i], sum


def process(depth):
    global area;
    global HAND;
    global g1,g2
    res=()
    H=depth.shape[0]
    W=depth.shape[1]
    square=depth[g2:H-g2,g1:W-g1]
    cv.imshow('Depth', square/1500)
    H=square.shape[0]
    W=square.shape[1]
    # push edge pixels to infinity:
    square[square==0]=4500
    cutoff, sum = computeCutoff(np.ravel(square), area)
    #print('cutoff %i %i'%(cutoff, sum))
    if 64 < sum and sum < 2*area:
        ground = 400
        weights=np.reciprocal((square-ground)/(cutoff-ground))
        weights[square>cutoff]=0.0
        Z,sx,sy,sxx,syy = computeMoments(weights, H, W)
        if Z > 0:
            Z = ground + (cutoff-ground) / Z
        if sxx + syy > 0:
            R = (syy-sxx)/(sxx+syy)
        else:
            R = 0
        #print("%7.2f %7.2f %7.2f %7.2f : %7.2f"%(sx,sy,sxx,syy,Z))

        X = max(0, min(1, (sx-g1)/(W-2*g1)))
        Y = max(0, min(1, 1-(sy-g2)/(H-2*g2)))
        print("%7.2f %7.2f %7.2f : %.2f %i"%(X,Y,Z,R,HAND))
        ###############
        cv.imshow('Weights',weights/max(np.ravel(weights)))
        #cv.circle(weights, (int(sx),int(sy)), 10, (0, 1, 1),2)
        ACT = 0
        margin = 0.25
        if HAND:
            if -R > margin:
                ACT = 2
                HAND = 0
        else:
            if R > margin:
                ACT = 1
                HAND = 1
        res = (X, Y, ACT)
    return res

############################################################
#Sensor initialization
from pylibfreenect2 import OpenGLPacketPipeline, CpuPacketPipeline
try:
    #pipeline = OpenGLPacketPipeline()
    pipeline = OpenCLPacketPipeline()
    print("OpenCL pipeline")
except:
    pipeline = CpuPacketPipeline()
    print("CPU pipeline")
    
fn = Freenect2()
num_devices = fn.enumerateDevices()
if num_devices == 0:
    print("No device connected!")
    sys.exit(1)

serial = fn.getDeviceSerialNumber(0)
device = fn.openDevice(serial, pipeline=pipeline)
#listener = SyncMultiFrameListener(FrameType.Color|FrameType.Ir|FrameType.Depth)
listener = SyncMultiFrameListener(FrameType.Depth)
#device.setColorFrameListener(listener)
device.setIrAndDepthFrameListener(listener)
device.start()

##########################################################
cv.namedWindow('Weights')
cv.moveWindow('Weights',20,20)
cv.namedWindow('Depth')

cv.createTrackbar('area', 'Depth', area, 2000, change_area)
cv.setTrackbarMin('area', 'Depth', 100)
cv.createTrackbar('g1', 'Depth', g1, 130, change_g1)
cv.createTrackbar('g2', 'Depth', g2, 130, change_g2)

pipe=os.open('fifo', os.O_WRONLY)
print("Pipe open @ %i"%pipe)

while True:
    frames = listener.waitForNewFrame()
    depth = frames["depth"].asarray().copy()
    #cv.imshow('Depth', depth/1500)
    listener.release(frames)
    a = process(depth)
    if a:
        B=b"%.5f %.5f %d\n"%(a[0], a[1], a[2])
        if pipe > 0:
            os.write(pipe, B)
            print('>'+B.decode(), end='')
        else:
            print('>'+B.decode(), end='')            
    if cv.waitKey(1)==27:
        break

device.stop()
device.close()
cv.destroyAllWindows()
sys.exit(0)
