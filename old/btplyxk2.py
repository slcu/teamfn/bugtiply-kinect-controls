import numpy as np
import cv2
import sys
#, os, io
from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame
from pylibfreenect2 import createConsoleLogger, setGlobalLogger
from pylibfreenect2 import LoggerLevel


#Names of windows to be dusplayed.
window_depth_name = 'Depth'
window_fulldepth_name = 'DepthFULL'

#Action and previous iteration action
HAND=0;
#number of pixels to be considered
area = 1000
g1=64
g2=128



#Parameters for closing operation
kernelSize = (7, 7)
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernelSize)

def change_g2(value):
    global g2
    g2 = value

def change_g1(value):
    global g1
    g1 = value

def change_area(value):
    global area
    area = value

def computeMoments(img, H, W):
    c=0
    s=0.
    sy=0
    sx=0
    sxx=0
    syy=0
    for i in range(H):
        for k in range(W):
            w=img[i,k]
            if w>0.0:
                c += 1
                s += w
                sx+=w*i
                sy+=w*k
                sxx+=w*i*i
                syy+=w*k*k
    if c>0:
        sx=sx/s
        sy=sy/s
        sxx=sxx/s-sx*sx
        syy=syy/s-sy*sy
        s = s/c
    return ( s, sy, sx, syy, sxx )

def computeCutoff(data, arg):
    bins = np.linspace(500, 1500, 100)
    hist, dummy = np.histogram(data, bins)
    i=0
    csum=0
    for k in hist:
        #print(i,k,csum)
        i += 1
        csum += k
        if csum >= arg:
            break
    return bins[i]


def show_depth2():

    #global kernelSize,kernel
    global area;#,w,p;
    global current_depths,HAND;
    global g1,g2
    s1=0.5
    s2=0.5
    #g1x=g1/100
    #g2x=g2/100
    res=()

    frames = listener.waitForNewFrame()
    depth = frames["depth"].asarray()

    H=depth.shape[0]
    W=depth.shape[1]
    #wx=int(g2)
    #hx=int(g1)

    square=depth[g2:H-g2,g1:W-g1]
    H=square.shape[0]
    W=square.shape[1]
    # push edge pixels to infinity:
    square[square==0]=4500
    th = computeCutoff(np.ravel(square), 1500)
    wex=0
    if th < area:
        wex=1
        weights=np.reciprocal(square-300)
        weights[square>th]=0.0
        Z,sx,sy,sxx,syy = computeMoments(weights, H, W)
        Z = 300 + 1/Z
        #print("%7.2f %7.2f %7.2f %7.2f : %7.2f"%(sx,sy,sxx,syy,Z))

        X=(sx-g1)/(W-2*g1);
        Y=(sy-g2)/(H-2*g2);
        ###############

        if X>0 and Y>0 and X<1 and Y<1:
            ACT = 0
            margin = 1.2
            if HAND:
                if syy * margin <= sxx:
                    ACT = 2
                    HAND = 0
            else:
                if sxx * margin <= syy:
                    ACT = 1
                    HAND = 1
            res = (X, Y, ACT)
            print("%7.2f %7.2f %7.2f : %i"%(X,Y,Z, HAND))

            #cv2.circle(weights, (int(sx),int(sy)), 10, (0, 0, 0),1)
            #cv2.imshow('DepthFULL',255*weights)
    listener.release(frames)
    if wex==1:
        cv2.circle(weights, (int(sx),int(sy)), 10, (0, 0, 0),1)
        cv2.imshow('DepthFULL',255*weights)
    cv2.imshow('Depth', depth/4500)
    return res

############################################################
#Sensor initialization
try:
    from pylibfreenect2 import OpenGLPacketPipeline
    pipeline = OpenGLPacketPipeline()

except:
    try:
        from pylibfreenect2 import OpenCLPacketPipeline
        pipeline = OpenCLPacketPipeline()
    except:
        from pylibfreenect2 import CpuPacketPipeline
        pipeline = CpuPacketPipeline()

fn = Freenect2()
num_devices = fn.enumerateDevices()
if num_devices == 0:
    print("No device connected!")
    sys.exit(1)

serial = fn.getDeviceSerialNumber(0)
device = fn.openDevice(serial, pipeline=pipeline)

listener = SyncMultiFrameListener(
    FrameType.Color | FrameType.Ir | FrameType.Depth)

# Register listeners
device.setColorFrameListener(listener)
device.setIrAndDepthFrameListener(listener)
device.start()
##########################################################
#Windows to be shown with parameters g1,g2 to scale the X and Y values and the small cutoff
cv2.namedWindow('DepthFULL')
cv2.moveWindow('DepthFULL',20,20)
cv2.namedWindow('Depth')
cv2.createTrackbar('area', 'Depth', area, 2000,  change_area)
cv2.setTrackbarMin('area', 'Depth', 100)

cv2.createTrackbar('g1', 'Depth', g1, 130, change_g1)
cv2.createTrackbar('g2', 'Depth', g2, 130, change_g2)

#Main Loop

u=True

#FIFO OR NOT BOOLEAN
pipe=True

if pipe:
    p='fifo'
##########

while u:
    a=show_depth2()
    if a:
        s="%.5f %.5f %d\n"%(a[0], a[1], a[2])
        if pipe:
            fw=open(p,'w')
            fw.write(s)
            fw.flush()
            fw.close()
        if (cv2.waitKey(10)==27):
            break
      
device.stop()
device.close()
cv2.destroyAllWindows()
sys.exit(0)
